// Copyright (c) 2015 tmrhmd.com
// All rights reserved. I'll sue your ass.

// Lookup IP address
var ipRegEx = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$|^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/

function lookupIP(ip) {
  if (ipRegEx.test(ip)) {
    var url = "http://www.infosniper.net/index.php?ip_address=" + encodeURIComponent(ip);
    window.open(url, '_blank');
  } else {
    alert('Invalid IP address or hostname!');
  }
}

// Lookup rate
function searchRate(number) {
  window.open("http://www.callcentric.com/find_rate.php?type=go&type1=go&t=FoJ7W7CUpp05I7yAaoMl&dial_phone=" + number, '_blank')
}

// Look up a person

// Pipl
function lookupPersonPipl(name) {
  var location = prompt('Enter a geographic location to search or click Cancel');
  if (location) {
    window.open("https://pipl.com/search/?q=" + encodeURIComponent(name) + "&l=" + encodeURIComponent(location), '_blank');
  } else {
    window.open("https://pipl.com/search/?q=" + encodeURIComponent(name), '_blank');
  }
}
// Google
function lookupPersonGoogle(name) {
  window.open("https://www.google.com/search?q=" + encodeURIComponent(name), '_blank');
}
// Facebook
function lookupPersonFacebook(name) {
  window.open("https://www.facebook.com/public?query=" + encodeURIComponent(name), '_blank');
}

// Search client
function searchClient(query) {
  window.open("https://admin.callcentric.com/client.php?stat=all&go=1&search=" + encodeURIComponent(query), '_blank');
}

// Search DID
function searchDID(query) {
  window.open("https://admin.callcentric.com/did_pool.php?go=1&stat=active&order=p.did+ASC&search=" + encodeURIComponent(query), '_blank');
}

// Search tickets
function searchTickets(query) {
  if (query) {
    window.open("https://admin.callcentric.com/tt_list.php?search=" + encodeURIComponent(query) + "&admin_id=&go=Go", '_blank');
  } else {
    var query = prompt("Enter a search term:");
    window.open("https://admin.callcentric.com/tt_list.php?search=" + encodeURIComponent(query), '_blank');
  }
}

// Search VoIPMonitor
function searchVoIPMonitor(query) {
  window.open("http://voipmonitor.callcentric.com/voipmonitor/admin.php?cdr_filter={%22fdatefrom%22:%222015-10-28%22,%22fcallid%22:%22" + encodeURIComponent(query) +"%22}&cdr_group_data={%22group_by%22:4}", '_blank');
}

// Search FAQ
function searchFAQ(query) {
  if (query) {
    window.open("http://www.callcentric.com/search.php?cx=011354805187833468149%3Akiiydxtyudy&cof=FORID%3A11%3BNB%3A1&ie=UTF-8&q=" + encodeURIComponent(query), '_blank');
  } else {
    var query = prompt("Enter a search term:");
    window.open("http://www.callcentric.com/search.php?cx=011354805187833468149%3Akiiydxtyudy&cof=FORID%3A11%3BNB%3A1&ie=UTF-8&q=" + encodeURIComponent(query), '_blank');
  }
}

// Google Translate
function translateText(query) {
  window.open("https://translate.google.com/#auto/en/" + encodeURIComponent(query));
}

// The onClicked callback function.
function onClickHandler(info, tab) {
  switch(info.menuItemId) {
    case 'lookupIP':
      lookupIP(info.selectionText);
      break;

    case 'lookupPersonPipl':
      lookupPersonPipl(info.selectionText);
      break;
    case 'lookupPersonGoogle':
      lookupPersonGoogle(info.selectionText);
      break;
    case 'lookupPersonFacebook':
      lookupPersonFacebook(info.selectionText);
      break;

    case 'searchClient':
      searchClient(info.selectionText);
      break;
    case 'searchDID':
      searchDID(info.selectionText);
      break;
    case 'searchTickets':
      searchTickets(info.selectionText);
      break;
    case 'searchRate':
      searchRate(info.selectionText);
      break;
    case 'searchVoIPMonitor':
      searchVoIPMonitor(info.selectionText);
      break;
    case 'searchFAQ':
      searchFAQ(info.selectionText);
      break;

    case 'translateText':
      translateText(info.selectionText);
      break;
  }
};

chrome.contextMenus.onClicked.addListener(onClickHandler);

// Set up context menu tree at install time.
chrome.runtime.onInstalled.addListener(function() {
  // Main context menu
  chrome.contextMenus.create({"title": "Callcentric", "contexts": ['selection', 'page'], "id": "mainMenu"})

  // Lookup menus
  chrome.contextMenus.create({"title": "Lookup", "contexts": ['selection'],  "parentId": "mainMenu", "id": "lookup"});
  chrome.contextMenus.create({"title": "IP address", "contexts": ['selection'],  "parentId": "lookup", "id": "lookupIP"});
  chrome.contextMenus.create({"title": "Translation", "contexts": ['selection'], "parentId": "lookup",  "id": "translateText"})

  chrome.contextMenus.create({"title": "Person", "contexts": ['selection'],  "parentId": "lookup", "id": "lookupPerson"});
  chrome.contextMenus.create({"title": "Pipl", "contexts": ['selection'],  "parentId": "lookupPerson", "id": "lookupPersonPipl"});
  chrome.contextMenus.create({"title": "Google", "contexts": ['selection'],  "parentId": "lookupPerson", "id": "lookupPersonGoogle"});
  chrome.contextMenus.create({"title": "Facebook", "contexts": ['selection'],  "parentId": "lookupPerson", "id": "lookupPersonFacebook"});

  // Search menus
  chrome.contextMenus.create({"title": "Search", "contexts": ['selection', 'page'],  "parentId": "mainMenu", "id": "search"});
  chrome.contextMenus.create({"title": "Client", "contexts": ['selection'],  "parentId": "search", "id": "searchClient"});
  chrome.contextMenus.create({"title": "DID", "contexts": ['selection'],  "parentId": "search", "id": "searchDID"});
  chrome.contextMenus.create({"title": "Rate", "contexts": ['selection'],  "parentId": "search", "id": "searchRate"});
  chrome.contextMenus.create({"title": "Tickets", "contexts": ['selection', 'page'],  "parentId": "search", "id": "searchTickets"});
  chrome.contextMenus.create({"title": "VoIPMonitor", "contexts": ['selection'],  "parentId": "search", "id": "searchVoIPMonitor"});
  chrome.contextMenus.create({"title": "FAQ", "contexts": ['selection', 'page'],  "parentId": "search", "id": "searchFAQ"});
});